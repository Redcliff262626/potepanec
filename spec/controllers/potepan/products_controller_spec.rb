require 'rails_helper'
RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let!(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    it "renders show template" do
      expect(response).to render_template :show
    end

    it 'assigns @product' do
      expect(assigns(:product)).to eq product
    end
  end
end
